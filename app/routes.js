// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'default',
      indexRoute: { onEnter: (nextState, replace) => replace('/login') },
    },
    {
      path: '/dashboard(/:filter)',
      name: 'dashboard',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Dashboard/reducer'),
          import('containers/Dashboard/sagas'),
          import('containers/Dashboard'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('dashboard', reducer.default);
          injectSagas('dashboard', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/settings/personal',
      name: 'personalDetails',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/PersonalDetails/reducer'),
          import('containers/PersonalDetails/sagas'),
          import('containers/PersonalDetails'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('personalDetails', reducer.default);
          injectSagas('personalDetails', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/settings/bank',
      name: 'bankDetails',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/BankDetails/reducer'),
          import('containers/BankDetails/sagas'),
          import('containers/BankDetails'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('bankDetails', reducer.default);
          injectSagas('bankDetails', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/settings/account',
      name: 'accountDetails',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/AccountDetails/reducer'),
          import('containers/AccountDetails/sagas'),
          import('containers/AccountDetails'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('accountDetails', reducer.default);
          injectSagas('accountDetails', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/application/:id',
      name: 'application',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Application/reducer'),
          import('containers/Application/sagas'),
          import('containers/Application'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('application', reducer.default);
          injectSagas('application', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/Verification/:id',
      name: 'verification',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Verification/reducer'),
          import('containers/Verification/sagas'),
          import('containers/Verification'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('verification', reducer.default);
          injectSagas('verification', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/pricing/:id',
      name: 'pricing',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Pricing/reducer'),
          import('containers/Pricing/sagas'),
          import('containers/Pricing'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('pricing', reducer.default);
          injectSagas('pricing', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/agreement/:id',
      name: 'agreement',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Agreement/reducer'),
          import('containers/Agreement/sagas'),
          import('containers/Agreement'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('agreement', reducer.default);
          injectSagas('agreement', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/finalization/:id',
      name: 'finalization',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Finalization/reducer'),
          import('containers/Finalization/sagas'),
          import('containers/Finalization'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('finalization', reducer.default);
          injectSagas('finalization', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/link/account/:id/:token',
      name: 'accountLinking',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/AccountLinking/reducer'),
          import('containers/AccountLinking/sagas'),
          import('containers/AccountLinking'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('accountLinking', reducer.default);
          injectSagas('accountLinking', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/login',
      name: 'login',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Login/reducer'),
          import('containers/Login/sagas'),
          import('containers/Login'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('login', reducer.default);
          injectSagas('login', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/declined/:id',
      name: 'declined',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Declined/reducer'),
          import('containers/Declined/sagas'),
          import('containers/Declined'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('declined', reducer.default);
          injectSagas('declined', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/funded/:id',
      name: 'funded',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Funded/reducer'),
          import('containers/Funded/sagas'),
          import('containers/Funded'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('funded', reducer.default);
          injectSagas('funded', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'forgot-password',
      name: 'forgotPassword',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ForgotPassword/reducer'),
          import('containers/ForgotPassword/sagas'),
          import('containers/ForgotPassword'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('forgotPassword', reducer.default);
          injectSagas('forgotPassword', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'reset-account/:token/:email',
      name: 'resetAccount',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ResetAccount/reducer'),
          import('containers/ResetAccount/sagas'),
          import('containers/ResetAccount'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('resetAccount', reducer.default);
          injectSagas('resetAccount', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/crisko/success/:token',
      name: 'criskoSuccess',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/CriskoSuccess/reducer'),
          import('containers/CriskoSuccess/sagas'),
          import('containers/CriskoSuccess'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('criskoSuccess', reducer.default);
          injectSagas('criskoSuccess', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
