/*
 * Agreement Messages
 *
 * This contains all the text for the Agreement component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Agreement.header',
    defaultMessage: 'This is Agreement container !',
  },
});
