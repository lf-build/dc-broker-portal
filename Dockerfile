# Pure nginx / html
# FROM funkygibbon/nginx-pagespeed
# ADD /build /app/www
# EXPOSE 80 443

FROM node

ARG NPM_TOKEN
# ADD /.npmrc /tmp/.npmrc
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN printf "//`node -p \"require('url').parse(process.env.NPM_REGISTRY_URL || 'https://registry.npmjs.org').host\"`/:_authToken=${NPM_TOKEN}\nregistry=${NPM_REGISTRY_URL:-https://registry.npmjs.org}\n" >> ~/.npmrc
RUN npm install
RUN rm -f .npmrc
ADD . /tmp/
EXPOSE 3000

ENTRYPOINT npm run start:production