/*
 * OwnerDetails Messages
 *
 * This contains all the text for the OwnerDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.OwnerDetails.header',
    defaultMessage: 'This is the OwnerDetails component !',
  },
  firstNameLabel: {
    id: 'app.components.OwnerDetails.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.OwnerDetails.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.OwnerDetails.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.OwnerDetails.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  ssnLabel: {
    id: 'app.components.OwnerDetails.ssnLabel',
    defaultMessage: 'SSN',
  },
  ssnPlaceholder: {
    id: 'app.components.OwnerDetails.ssnPlaceholder',
    defaultMessage: 'SSN',
  },
  primaryPhoneLabel: {
    id: 'app.components.OwnerDetails.primaryPhoneLabel',
    defaultMessage: 'Primary Phone',
  },
  primaryPhonePlaceholder: {
    id: 'app.components.OwnerDetails.primaryPlaceholder',
    defaultMessage: 'Primary Phone #',
  },
  secondaryPhoneLabel: {
    id: 'app.components.OwnerDetails.secondaryPhoneLabel',
    defaultMessage: 'Secondary Phone',
  },
  secondaryPhonePlaceholder: {
    id: 'app.components.OwnerDetails.secondaryPlaceholder',
    defaultMessage: 'Secondary Phone #',
  },
  emailAddressLabel: {
    id: 'app.components.OwnerDetails.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  emailAddressPlaceholder: {
    id: 'app.components.OwnerDetails.emailAddressPlaceholder',
    defaultMessage: 'Email Address',
  },
  homeAddressLabel: {
    id: 'app.components.BusinessDetails.homeAddressLabel',
    defaultMessage: 'Home Address',
  },
  homeAddressPlaceholder: {
    id: 'app.components.BusinessDetails.homeAddressPlaceholder',
    defaultMessage: 'Home Address',
  },
  cityLabel: {
    id: 'app.components.BusinessDetails.city',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.BusinessDetails.city',
    defaultMessage: 'City',
  },
  stateLabel: {
    id: 'app.components.BusinessDetails.state',
    defaultMessage: 'State',
  },
  zipCodeLabel: {
    id: 'app.components.BusinessDetails.zipCode',
    defaultMessage: 'Zip Code',
  },
  zipCodePlaceholder: {
    id: 'app.components.BusinessDetails.zipCode',
    defaultMessage: 'Zip Code',
  },
});
