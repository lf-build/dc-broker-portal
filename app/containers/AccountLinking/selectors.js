import { createSelector } from 'reselect';

/**
 * Direct selector to the accountLinking state domain
 */
const selectAccountLinkingDomain = () => (state) => state.get('accountLinking');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AccountLinking
 */

const makeSelectAccountLinking = () => createSelector(
  selectAccountLinkingDomain(),
  (substate) => substate.toJS()
);

export default makeSelectAccountLinking;
export {
  selectAccountLinkingDomain,
};
