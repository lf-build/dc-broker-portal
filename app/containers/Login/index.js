/*
 *
 * Login
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { browserHistory, Link } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import Layout from 'components/PublicLayout';
import AlertMessage from 'components/AlertMessage';
import Spinner from 'components/Spinner';
import ssl from 'assets/images/ssl.png';
import soc from 'assets/images/soc.jpg';
import { isEmail } from 'lib/validation/isEmail';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import { userSignedIn, loadUserProfile, userSignedOut } from 'sagas/auth/actions';
import makeSelectLogin from './selectors';
import messages from './messages';

export class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, userName: '', password: '', showSpinner: false };
  }
  componentDidMount() {
    this.props.dispatch(userSignedOut());
  }
  render() {
    const { dispatch } = this.props;
    const modifyJason = (values) => {
      this.setState({
        userName: values.username,
        password: values.password,
        showSpinner: true,
      });
      const request = {
        ...values,
        realm: 'broker-portal',
      };
      return request;
    };
    const afterSubmit = (err, values) => {
      if (err) {
        this.setState({ isError: true, message: 'Invalid username or password', isVisible: true, showSpinner: false });
        return;
      }
      dispatch(userSignedIn(values.token));
      dispatch(loadUserProfile({ userId: values.userId, userName: this.state.userName, partnerId: values.partnerId }));
    };
    const moveToForgotPassword = () => {
      browserHistory.replace('/forgot-password');
    };
    const parsedMessages = localityNormalizer(messages);
    return (
      <Layout>
        <div className="clearfix"></div>
        <div className="container">
          <div className="login_wrap">
            <div className="col-md-6 col-sm-8 main_login">
              <h3>Broker Portal Log-in</h3>
              <div className="main_logininner">
                <Form initialValuesBuilder={() => ({ username: this.state.userName, password: this.state.password })} afterSubmit={afterSubmit} name="loginForm" action="authorization/identity/login" payloadBuilder={modifyJason}>
                  <TextField className="login_input" name="username" {...parsedMessages.email} validate={[required('Email'), isEmail('Email')]} />
                  <TextField type="password" className="login_input" name="password" {...parsedMessages.password} validate={[required('Password'), isInLength(5, 20)]} />
                  <div className="checkbox col-lg-6 no-pad ">
                    <label htmlFor="rememberMe"><input name="rememberMe" type="checkbox" /> Remember me</label>
                  </div>
                  <div className="checkbox col-lg-6">
                    <p className="forgot-pass"><Link onClick={() => moveToForgotPassword()}>Forgot password?</Link></p>
                  </div>
                  <button type="submit" className="loin_btn">Log In</button>
                  <p className="signup-txt">{'Don\'t have an account?'}<Link target="_blank" to="https://divergecap.com/partners/"> Click here to sign up</Link></p>
                </Form>
                { this.state.showSpinner && <Spinner /> }
                {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
              </div>
            </div>
            <div className="col-md-3 col-sm-4 login_right">
              <div className="login_rightinner margin2">
                <h3 className="login-que"> Have questions?</h3>
                <p className="call-at">Give us a call at: (888) 381-0532</p>
                <p className="email-t">or send us an email:
                  <a className="iso-text" href="mailto:iso@divergecap.com"> iso@divergecap.com</a>
                </p>
                <div>
                  <img className="ssl" src={ssl} alt="" />
                  <img className="soc" src={soc} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Login: makeSelectLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
