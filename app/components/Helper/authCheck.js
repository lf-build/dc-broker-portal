import { browserHistory } from 'react-router';

export function authCheck(e) {
  if (e && e.status && e.status.code === 401) {
    browserHistory.replace('/login');
  }
}
