/*
 *
 * AccountDetails
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Layout from 'containers/AppLayout';

import makeSelectAccountDetails from './selectors';
import AccountDetailsSetting from '../../components/AccountDetailsSetting';

export class AccountDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Layout>
        <div className="application_wraper">
          <AccountDetailsSetting />
        </div>
      </Layout>
    );
  }
}

AccountDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  AccountDetails: makeSelectAccountDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountDetails);
