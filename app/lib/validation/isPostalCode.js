import isRegexMatch from './isRegexMatch';
import { postalCodeRegex } from './regexList';

export const isPostalCode = (name) => (value) => isRegexMatch(value, postalCodeRegex) ? undefined : `Invalid ${name}`;
