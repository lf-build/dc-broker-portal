/*
 * Finalization Messages
 *
 * This contains all the text for the Finalization component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Finalization.header',
    defaultMessage: 'This is Finalization container !',
  },
});
