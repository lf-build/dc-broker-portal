/*
 * AccountDetailsSetting Messages
 *
 * This contains all the text for the AccountDetailsSetting component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AccountDetailsSetting.header',
    defaultMessage: 'This is the AccountDetailsSetting component !',
  },
  emailPlaceholder: {
    id: 'app.components.AccountDetailsSetting.emailPlaceholder',
    defaultMessage: 'Email',
  },
  emailLabel: {
    id: 'app.components.AccountDetailsSetting.emailTitle',
    defaultMessage: 'Email',
  },
});
