export function downloadFile(fileName, content) {
  const a = document.createElement('a');
  const mimeType = 'data:application/octet-stream;base64,';
  const data = `${mimeType}${content}`;
  a.href = data;
  a.download = fileName;
  a.click();
}

const defaultMaxFileSize = 2097152; // in bytes
export function IsUnderFileSizeLimit(fileList, maxSize) {
  let fileSizeLimit = defaultMaxFileSize;
  if (maxSize) {
    fileSizeLimit = maxSize;
  }
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (fileObject.size > fileSizeLimit) {
      return false;
    }
  }
  return true;
}

const defaultValidFileFormat = ['image/png', 'image/jpeg', 'application/pdf'];
export function IsValidFileFormat(fileList) {
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (defaultValidFileFormat.filter((p) => p === fileObject.type).length === 0) {
      return false;
    }
  }
  return true;
}

const defaultValidImageFormat = ['image/png', 'image/jpeg'];
export function IsValidImageFormat(fileList) {
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (defaultValidImageFormat.filter((p) => p === fileObject.type).length === 0) {
      return false;
    }
  }
  return true;
}

export function getInitialName(fullName) {
  let response = '';
  if (fullName) {
    const splitedName = fullName.split(' ');
    response = splitedName.length === 0 ? '' : `${splitedName.length > 0 ? splitedName[0][0] : ''}${splitedName.length > 1 ? splitedName[1][0] : ''}`;
  }
  return response;
}
