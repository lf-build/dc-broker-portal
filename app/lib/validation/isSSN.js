import isRegexMatch from './isRegexMatch';
import { ssnRegex } from './regexList';

export const isSSN = (name) => (value) => isRegexMatch(value, ssnRegex) ? undefined : `Invalid ${name}`;
