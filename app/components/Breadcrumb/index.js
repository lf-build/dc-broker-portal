/**
*
* Breadcrumb
*
*/

import React from 'react';
// import styled from 'styled-components';

class Breadcrumb extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { applicationStatus } = this.props;
    let application = '';
    let verification = '';
    let pricing = '';
    let agreement = '';
    let finalization = '';
    switch (applicationStatus) {
      case '0':
        application = 'active';
        break;
      case '1':
        application = 'completed';
        verification = 'active';
        break;
      case '2':
        application = verification = 'completed';
        pricing = 'active';
        break;
      case '3':
        application = verification = pricing = 'completed';
        agreement = 'active';
        break;
      case '4':
        application = verification = pricing = agreement = 'completed';
        finalization = 'active';
        break;
      default:
        break;
    }
    return (
      <div className="menu-wrap">
        <h3 className="status"> Status:</h3>
        <ul className="menu-list">
          <li id="me1" className={application}><a> Application</a></li>
          <li id="me2" className={verification}><a> Verification</a></li>
          <li id="me3" className={pricing}><a> Pricing</a></li>
          <li id="me4" className={agreement}><a> Agreement</a></li>
          <li id="me5" className={finalization}><a> Finalization</a></li>
        </ul>
      </div>
    );
  }
}

Breadcrumb.propTypes = {
  applicationStatus: React.PropTypes.any,
};

export default Breadcrumb;
