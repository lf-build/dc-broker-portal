/**
*
* ApplicationMatrixLine
*
*/

import React from 'react';
import ApplicationMatrixCount from 'components/ApplicationMatrixCount';
import ApplicationMatrixCommissionSummary from 'components/ApplicationMatrixCommissionSummary';
// import styled from 'styled-components';

// import {FormattedMessage} from 'react-intl';
// import messages from './messages';

class ApplicationMatrixLine extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { matrix, short, commission, title } = this.props;
    return (
      <div className="row">
        <div className=" col-lg-7 col-md-8 col-sm-12 col-xs-12">
          <h3 className={short ? 'youelife_section' : ''}> {title} </h3>
          <div className="row">
            { matrix.map((p, index) => (<div key={index} className=" col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <ApplicationMatrixCount {...p} short={short} />
            </div>
              )) }
          </div>
        </div>
        <div className=" col-lg-5 col-md-4 col-sm-12 col-xs-12">
          <div className="row">
            <div className=" col-lg-3 col-md-1 col-sm-12 col-xs-12"></div>
            <div className=" col-lg-6 col-md-10 col-sm-4 col-xs-12">
              <ApplicationMatrixCommissionSummary short={short} commission={commission} />
            </div>
            <div className=" col-lg-3 col-md-1 col-sm-12 col-xs-12"></div>
          </div>
        </div>
      </div>
    );
  }
}
ApplicationMatrixLine.propTypes = {
  commission: 0,
  matrix: [],
};

ApplicationMatrixLine.propTypes = {
  commission: React.PropTypes.number,
  short: React.PropTypes.bool,
  matrix: React.PropTypes.array,
  title: React.PropTypes.string.isRequired,
};

export default ApplicationMatrixLine;
