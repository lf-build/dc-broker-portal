/*
 * PersonalDetails Messages
 *
 * This contains all the text for the PersonalDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PersonalDetails.header',
    defaultMessage: 'This is PersonalDetails container !',
  },
});
