/**
*
* ClientEmailVerification
*
*/

import React from 'react';
import numberIcon from 'assets/images/number-icon.png';
import emailIcon from 'assets/images/email-icon.png';

class ClientEmailVerification extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { phoneNumber, emailStatus } = this.props;
    let activeClass = 'verify-box active';
    if (!emailStatus) {
      activeClass = 'verify-box';
    }
    return (
      <div className={activeClass}>
        <div className="number-box">
          <span>1</span>
        </div>
        <div className="verify-head">
          <h4>We’ve sent your client an email on your behalf.</h4>
        </div>

        <div className="verify-img">
          <a><img src={emailIcon} alt="" /></a>
          <a>View email </a>
        </div>

        <div className="verify-detail">
          <p>Bring it to their attention to speed up the process</p>
          <div className="v-number">
            <img src={numberIcon} alt="" />
            <span>{phoneNumber}</span>
          </div>
        </div>

      </div>
    );
  }
}

ClientEmailVerification.propTypes = {
  phoneNumber: React.PropTypes.string,
  emailStatus: React.PropTypes.bool,
};

export default ClientEmailVerification;
