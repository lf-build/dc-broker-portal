/*
 *
 * Application
 *
 */

import React, { PropTypes } from 'react';
import lodash from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { browserHistory, withRouter } from 'react-router';
import { Field, reset, submit, change } from 'redux-form/immutable';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import Form from '@ui/Form';
import MoneyField from '@ui/MoneyField';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/AlertMessage';
import { authCheck } from 'components/Helper/authCheck';
import Breadcrumb from 'components/Breadcrumb';
import OwnerDetails from 'components/OwnerDetails';
import BusinessDetails from 'components/BusinessDetails';
import makeAuthSelector from 'sagas/auth/selectors';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import makeSelectApplication from './selectors';
import messages from './messages';


import validateForm from './validate';
const renderConsentField = ({ input, label, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className="checkbox">
    <label htmlFor="consents"> <input {...input} placeholder={label} type={type} />I agree to the <a target="_blank" href="http://divergecap.com/terms-of-use/"> Terms of Service </a>&amp; <a target="_blank" href="http://divergecap.com/privacy-policy/"> Privacy Policy.</a> </label>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);
const ButtonPanel = (props) => {
  const { submitting ,saveDraft, discardApplication} = props; // eslint-disable-line
  return (<div className="comman_applicationinner business-details">
    <div className=" col-lg-2 col-md-3 col-sm-6 col-xs-12">
      <button type="button" disabled={submitting} className="btn" onClick={() => discardApplication()}>Discard</button>
    </div>
    <div className=" col-lg-4 col-md-4  hidden-sm col-xs-12">
    </div>
    <div className=" col-lg-2 col-md-3 col-sm-6  col-xs-12">
      <button type="button" disabled={submitting} className="btn" onClick={() => saveDraft()}>Save Draft</button>
    </div>
    <div className=" col-lg-2 col-md-3 col-sm-6 col-xs-12">
      <button type="submit" disabled={submitting} className="btn next_btn">Next</button>
    </div>
    <div className=" col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
  </div>);
};

export class Application extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isError: false, isVisible: false, message: '' };
  }
  saveAsDraft = false;
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    finalValues.owners = values.owners.map((owner) => {
      const finalOwner = owner;
      if (owner && owner.ssn) {
        finalOwner.ssn = owner.ssn.replace(/-/g, '');
      }
      if (owner && owner.primaryPhone) {
        finalOwner.primaryPhone = owner.primaryPhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
      }
      if ((owner && owner.secondaryPhone)) {
        finalOwner.secondaryPhone = owner.secondaryPhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
      }
      return finalOwner;
    });
    if (values && values.officePhone) {
      finalValues.officePhone = values.officePhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
    }
    if (values && values.requestedAmount) {
      if (isNaN(values.requestedAmount)) {
        if (values.requestedAmount.indexOf('$') !== -1 && values.requestedAmount.indexOf(',') !== -1) {
          finalValues.requestedAmount = parseInt(values.requestedAmount.replace(/[$, ]/g, ''), 10);
        }
      } else {
        finalValues.requestedAmount = values.requestedAmount;
      }
    }
    finalValues.partnerUserId = this.props.auth.user.userId;
    finalValues.applicationNumber = this.props.location.pathname.split('/')[2] === 'new' ? undefined : this.props.location.pathname.split('/')[2];
    if (this.saveAsDraft) {
      finalValues['@@action'] = 'on-boarding-broker/application/set-application-draft';
    }
    delete finalValues.draft;

    // Remove null, undefined from payload object
    const payload = lodash.pickBy(finalValues, lodash.identity);
    const owners = payload.owners.map((item) => lodash.pickBy(item, lodash.identity));
    delete payload.owners;
    payload.owners = owners;

    return payload;
  };

  afterSubmit=(err, data) => {
    if (err) {
      this.props.dispatch(change('applicationForm', 'draft', false));
      this.state.isError = true;
      this.state.isVisible = true;
      this.state.message = 'Something went wrong!';
      this.setState(this.state);
      this.props.dispatch(reset('applicationForm'));
    } else if (data) {
      if (data.draft) {
        browserHistory.replace('/dashboard');
      } else {
        browserHistory.replace(`/verification/${data.applicationNumber}`);
      }
    }
  }

  saveDraft = () => {
    this.saveAsDraft = true;
    this.props.dispatch(change('applicationForm', 'draft', (new Date()).getTime()));
    setTimeout(() => {
      this.props.dispatch(submit('applicationForm'));
    });
  };
  discardApplication = () => {
    const payLoad = {
      applicationNumber: this.props.location.pathname.split('/')[2],
      rejectCode: '700.40',
    };
    if (this.props.location.pathname.split('/')[2] !== 'new') {
      execute(undefined, undefined, ...'on-boarding-broker/application/discard-application'.split('/'), payLoad)
    .then(() => {
      browserHistory.replace('/dashboard');
    }).catch((e) => {
      authCheck(e);
      this.state.isError = true;
      this.state.isVisible = true;
      this.state.message = 'Something went wrong!';
      this.setState(this.state);
    }); // eslint-disable-line
    } else {
      this.props.dispatch(reset('applicationForm'));
    }

    this.props.dispatch(reset('applicationForm'));
    this.setState({ activeIndex: 0 });
  };
  initialValues = (value) => {
    const returnValue = {
      owners: [{}],
    };
    if (value) {
      return value;
    }
    return returnValue;
  };
  handleOnLoadError = (err) => {
    authCheck(err);
  };
  render() {
    if (!this.state) {
      return <Spinner />;
    }
    const parsedMessages = localityNormalizer(messages);
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <div className="col-xs-12">
              <h2 className="status-heading">New Application</h2>
            </div>
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="0" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <Form passPropsTo="ButtonPanel" loadAction={['on-boarding-broker/application/get-application', { applicationNumber: this.props.location.pathname.split('/')[2] }]} afterSubmit={(err, data) => this.afterSubmit(err, data)} initialValuesBuilder={(value) => this.initialValues(value)} payloadBuilder={(values) => this.modifyJson(values)} validate={(values) => validateForm(this, values)} name="applicationForm" action="on-boarding-broker/application/set-application" onLoadError={this.handleOnLoadError}>
                <OwnerDetails />
                <BusinessDetails />
                <div className="comman_applicationinner business-details">
                  <div className=" col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <MoneyField name="requestedAmount" {...parsedMessages.requestedAmount} />
                  </div>
                  <div className=" col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <Field type="input" name="draft" component="hidden" value="false" />
                  </div>
                  <div className=" col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  </div>
                  <div className=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <Field name="consents" component={renderConsentField} type="checkbox" />
                  </div>
                  <div className=" col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                  </div>
                </div>
                <ButtonPanel saveDraft={this.saveDraft} discardApplication={this.discardApplication} />
              </Form>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Application.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Application: makeSelectApplication(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Application));
