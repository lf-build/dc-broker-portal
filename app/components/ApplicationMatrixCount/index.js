/**
*
* ApplicationMatrixCount
*
*/

import React from 'react';
// import styled from 'styled-components';

class ApplicationMatrixCount extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { count, text, icon, short } = this.props;
    if (short) {
      return (
        <div className="widget_section">
          <a className={icon}>
            {count}
          </a>
        </div>
      );
    }
    return (
      <div className="widget_section">
        <div className={icon}>
          <h1>{count}</h1>
          <p>{text}</p>
        </div>
      </div>
    );
  }
}

ApplicationMatrixCount.propTypes = {
  count: React.PropTypes.number,
  text: React.PropTypes.string,
  icon: React.PropTypes.string,
  short: React.PropTypes.bool,
};

export default ApplicationMatrixCount;
