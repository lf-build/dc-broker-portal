/**
*
* FileDropZone
*
*
**/

import React from 'react';
import Files from 'components/Files';
import Dropzone from 'react-dropzone';

class FileDropZone extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { files, onDrop, applicationNumber, onDiscard, uploadFileType } = this.props;
    const handleFileDropped = (blobs) => {
      const promises = blobs.map((file) => new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          resolve({ file: file.name, type: file.type, dataUrl: e.target.result, size: file.size });
        };
        reader.readAsDataURL(file);
      }));
      Promise.all(promises).then((...dataUrls) => onDrop(dataUrls));
    };
    return (
      <div>
        <Files files={files} applicationNumber={applicationNumber} onDiscardFile={() => onDiscard()} uploadFileType={uploadFileType} />
        <Dropzone className="drag-text" onDrop={(blobs) => handleFileDropped(blobs)} >
          <span>Drag files here or</span>
          <a>upload documents individually</a>
        </Dropzone>
      </div>
    );
  }
}

FileDropZone.propTypes = {
  files: React.PropTypes.array.isRequired,
  onDrop: React.PropTypes.func.isRequired,
  // onRemove: React.PropTypes.func.isRequired,
  applicationNumber: React.PropTypes.string,
  // multiple: React.PropTypes.bool.isRequired,
  onDiscard: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
};

export default FileDropZone;
