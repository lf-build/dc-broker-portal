/*
 *
 * PersonalDetails
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Layout from 'containers/AppLayout';
import makeSelectPersonalDetails from './selectors';
import PersonalDetailsSetting from '../../components/PersonalDetailsSetting';

export class PersonalDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Layout>
        <div className="application_wraper">
          <PersonalDetailsSetting />
        </div>
      </Layout>
    );
  }
}

PersonalDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({ PersonalDetails: makeSelectPersonalDetails() });

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetails);
