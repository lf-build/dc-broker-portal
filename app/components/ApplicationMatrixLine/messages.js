/*
 * ApplicationMatrixLine Messages
 *
 * This contains all the text for the ApplicationMatrixLine component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ApplicationMatrixLine.header',
    defaultMessage: 'This is the ApplicationMatrixLine component !',
  },
});
