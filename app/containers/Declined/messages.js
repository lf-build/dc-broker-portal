/*
 * Declined Messages
 *
 * This contains all the text for the Declined component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Declined.header',
    defaultMessage: 'This is Declined container !',
  },
});
