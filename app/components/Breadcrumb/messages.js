/*
 * Breadcrumb Messages
 *
 * This contains all the text for the Breadcrumb component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Breadcrumb.header',
    defaultMessage: 'This is the Breadcrumb component !',
  },
});
