export default (a, b, strict) => {
  if (a instanceof Date && b instanceof Date) {
    return a.getTime() === a.getTime();
  }

  if (strict) {
    return a === b;
  }
  if (typeof a === 'string' && a && b) {
    return a.trim().toLowerCase() === b.trim().toLowerCase();
  }
  return a == b; //eslint-disable-line
};
