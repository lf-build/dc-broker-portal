/*
 * BankDetails Messages
 *
 * This contains all the text for the BankDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.BankDetails.header',
    defaultMessage: 'This is BankDetails container !',
  },
});
