/**
*
* Broker
*
*/

import React from 'react';
import { getInitialName } from 'components/Helper/functions';
import phoneBox from '../../assets/images/mobile-icon.png';
import emailBox from '../../assets/images/msg-icon.png';

// import styled from 'styled-components';


class Broker extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { name, email, phone, image } = this.props.broker;
    const initialName = getInitialName(name);
    return (
      <div className="col-md-6 account-contain">
        <div className="account-logos">
          <br />
          <br />
          <br />
          <br />
          <br />
          <a className="tooltip-b" href={`mailto:${email}`}><img src={emailBox} alt="" /><span className="tooltiptext">{email}</span></a>
          <a className="link-logo">
            { image !== null ? <img src={`${image}?${Math.random()}`} alt="" /> :
            <span className="logo-text">{initialName}</span>}
          </a>
          <a className="tooltip-b" href={`tel:${phone}`}><img src={phoneBox} alt="" /><span className="tooltiptext">{phone}</span></a>
        </div>

        <div className="question-wrap">
          <h4>Broker</h4>
          <p>{name}</p>
          <p>Have a question? Let us know!</p>
        </div>
      </div>
    );
  }
}

Broker.propTypes = {
  broker: React.PropTypes.any,
};

export default Broker;
