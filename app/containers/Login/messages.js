/*
 * Login Messages
 *
 * This contains all the text for the Login component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Login.header',
    defaultMessage: 'This is Login container !',
  },
  emailPlaceholder: {
    id: 'app.components.AccountDetailsSetting.emailPlaceholder',
    defaultMessage: 'Enter Your Email',
  },
  passwordPlaceholder: {
    id: 'app.components.AccountDetailsSetting.passwordPlaceholder',
    defaultMessage: 'Enter Your Password',
  },
});
