/*
 * PricingBox Messages
 *
 * This contains all the text for the PricingBox component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PricingBox.header',
    defaultMessage: 'This is the PricingBox component !',
  },
});
