import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BankDetailsSetting.header',
    defaultMessage: 'This is the BankDetailsSetting component !',
  },
  companyNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.companyNamePlaceholder',
    defaultMessage: 'Company Name',
  },
  companyNameLabel: {
    id: 'app.components.BankDetailsSetting.companyNameTitle',
    defaultMessage: 'Company Name',
  },
  bankNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.bankNamePlaceholder',
    defaultMessage: 'Bank Name',
  },
  bankNameLabel: {
    id: 'app.components.BankDetailsSetting.bankNameTitle',
    defaultMessage: 'Bank Name',
  },
  bankAccountNamePlaceholder: {
    id: 'app.components.BankDetailsSetting.bankAccountNamePlaceholder',
    defaultMessage: 'Bank Account Name',
  },
  bankAccountNameLabel: {
    id: 'app.components.BankDetailsSetting.bankAccountNameTitle',
    defaultMessage: 'Bank Account Name',
  },
  bankAccountNumberPlaceholder: {
    id: 'app.components.BankDetailsSetting.bankAccountNumberPlaceholder',
    defaultMessage: 'Bank Account Number',
  },
  bankAccountNumberLabel: {
    id: 'app.components.BankDetailsSetting.bankAccountNumberTitle',
    defaultMessage: 'Bank Account Number',
  },
  abaPlaceholder: {
    id: 'app.components.BankDetailsSetting.abaPlaceholder',
    defaultMessage: 'ABA #',
  },
  abaLabel: {
    id: 'app.components.BankDetailsSetting.abaTitle',
    defaultMessage: 'ABA #',
  },
});
