
import { fromJS } from 'immutable';
import verificationReducer from '../reducer';

describe('verificationReducer', () => {
  it('returns the initial state', () => {
    expect(verificationReducer(undefined, {})).toEqual(fromJS({}));
  });
});
