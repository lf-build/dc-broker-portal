
import { fromJS } from 'immutable';
import declinedReducer from '../reducer';

describe('declinedReducer', () => {
  it('returns the initial state', () => {
    expect(declinedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
