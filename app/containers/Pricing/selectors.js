import { createSelector } from 'reselect';

/**
 * Direct selector to the pricing state domain
 */
const selectPricingDomain = () => (state) => state.get('pricing');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Pricing
 */

const makeSelectPricing = () => createSelector(
  selectPricingDomain(),
  (substate) => substate.toJS()
);

export default makeSelectPricing;
export {
  selectPricingDomain,
};
