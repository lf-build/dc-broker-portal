/**
*
* ApplicantDetail
*
*/

import React from 'react';
import { browserHistory, withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { phoneMasking } from 'components/Helper/formatting';
import makeAuthSelector from 'sagas/auth/selectors';

class ApplicantDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/application/applicant-details'.split('/'), {
      applicationNumber: this.props.location.pathname.split('/')[2],
    })
    .then(({ body }) => {
      if (this.props.auth.user.partnerId !== body.partnerId) {
        browserHistory.replace('/dashboard');
        return;
      }
      this.setState({ legalBusinessName: body.legalBusinessName,
        applicationNumber: body.applicationNumber,
        applicantName: body.applicantName,
        email: body.email,
        phoneNumber: phoneMasking(body.phoneNumber),
      });
    }).catch((e) => browserHistory.replace('/dashboard')); // eslint-disable-line
  }
  render() {
    return (
      <div className="col-xs-12">
        <h2 className="status-heading">{this.state.legalBusinessName}</h2>
        <ul className="header_staus">
          <li><a>Application {this.state.applicationNumber}</a></li>
          <li><a>{this.state.applicantName}</a></li>
          <li><a>{this.state.email}</a></li>
          <li><a className="remove_rightborder">{this.state.phoneNumber}</a></li>
        </ul>
      </div>
    );
  }
}

ApplicantDetail.propTypes = {
  location: React.PropTypes.object.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

export default connect(mapStateToProps)(withRouter(ApplicantDetail));
