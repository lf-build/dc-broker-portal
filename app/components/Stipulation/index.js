/**
*
* Stipulation
*
*/

import React from 'react';
import FileDropZone from 'components/FileDropZone';
import AlertMessage from 'components/AlertMessage';
import Spinner from 'components/Spinner';
import { authCheck } from 'components/Helper/authCheck';
import { IsUnderFileSizeLimit, IsValidFileFormat } from 'components/Helper/functions';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';

class Stipulation extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, showSpinner: props.showSpinner };
  }
  render() {
    const { files } = this.props.Stipulation;
    const { applicationNumber, uploadFileType, documentNameList, subHeader, mainHeader, onDropSuccess } = this.props;
    const OnDropDone = (acceptedFiles) => {
      if (acceptedFiles[0].length > 1) {
        this.setState({ isError: true, message: 'Upload 1 file at a time', isVisible: true });
        return;
      }
      if (!IsValidFileFormat(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'Upload only PDF/JPG/PNG file', isVisible: true });
        return;
      }
      if (!IsUnderFileSizeLimit(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'Max file size is 2MB', isVisible: true });
        return;
      }
      execute(undefined, undefined, ...'on-boarding-broker/agreement/upload-engine'.split('/'), {
        applicationNumber,
        uploadFileType,
        fileList: acceptedFiles[0],
      })
      .then(() => {
        this.setState({ isVisible: false, showSpinner: true });
        onDropSuccess().then(() => this.setState({ showSpinner: false }));
      }).catch((e) => {
        authCheck(e);
      }); // eslint-disable-line
    };
    if (uploadFileType === 'signeddocument') {
      return (
        <div className="col-sm-4">
          <div className="contract-box">
            <div className="doc-txt">
              <p>{subHeader}</p>
            </div>
            <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} />
            {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
            { this.state.showSpinner && <Spinner /> }
          </div>
        </div>
      );
    }
    return (
      <div className="agree-wrap">
        <div className="agree-head">
          <h3>{mainHeader}</h3>
        </div>
        <div className="check-id">
          <div className="up-head">
            <h5>{subHeader}</h5>
          </div>
          <div className="up-text">
            <p>{documentNameList}</p>
          </div>
          <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} />
          { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
          { this.state.showSpinner && <Spinner /> }
        </div>
      </div>
    );
  }
}

Stipulation.propTypes = {
  Stipulation: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDropSuccess: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  mainHeader: React.PropTypes.string,
  subHeader: React.PropTypes.string,
  documentNameList: React.PropTypes.string,
  showSpinner: React.PropTypes.bool,
};

export default Stipulation;
