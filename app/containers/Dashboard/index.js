/*
 *
 * Dashboard
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { FormattedMessage, FormattedDate, FormattedNumber } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { browserHistory, Link, withRouter } from 'react-router';
import Layout from 'containers/AppLayout';
import ApplicationMatrix from 'components/ApplicationMatrix';
import { authCheck } from 'components/Helper/authCheck';
import makeAuthSelector from 'sagas/auth/selectors';
import DataGrid from '@ui/DataGrid';
import makeSelectDashboard from './selectors';
import messages from './messages';

const prepareRedirectionUrl = (applicationNumber, applicationStatus) => {
  let urlTo = '';
  switch (applicationStatus.trim()) {
    case 'Draft':
      urlTo = `/application/${applicationNumber}`;
      break;
    case 'Application Submitted':
    case 'Financial Verification':
    case 'Personal Verification':
    case 'Credit Verification':
      urlTo = `/verification/${applicationNumber}`;
      break;
    case 'Pricing':
      urlTo = `/pricing/${applicationNumber}`;
      break;
    case 'Agreement Docs Out':
    case 'Funding Review':
      urlTo = `/agreement/${applicationNumber}`;
      break;
    case 'Approved For Funding':
      urlTo = `/finalization/${applicationNumber}`;
      break;
    case 'Funded':
      urlTo = `/funded/${applicationNumber}`;
      break;
    case 'Cancelled':
    case 'Not Interested':
    case 'Declined':
    case 'Expired':
    case 'Discarded':
      urlTo = `/declined/${applicationNumber}`;
      break;
    default:
      urlTo = '/dashboard';
  }
  return urlTo;
};

const columnConfiguration = [{
  value: (row) => <Link to={prepareRedirectionUrl(row.ApplicationNumber, row.StatusName)}> {row.ApplicationNumber} </Link>,
  key: 'App ID',
  sortBy: 'ApplicationNumber',
  searchBy: 'ApplicationNumber',
}, {
  value: (row) => <FormattedDate
    value={new Date(row.Submitted.Time.DateTime)}
    year="numeric"
    month="long"
    day="2-digit"
  />,
  key: 'Submitted',
  sortBy: 'Submitted.Time.Ticks',
}, {
  value: 'BusinessApplicantName',
  key: 'Applicant',
  sortBy: 'BusinessApplicantName',
  searchBy: 'BusinessApplicantName',
}, {
  value: 'LegalBusinessName',
  key: 'Business',
  sortBy: 'LegalBusinessName',
  searchBy: 'LegalBusinessName',
}, {
      value: (row) => <FormattedNumber value={row.RequestedAmount} style="currency" currency="USD" />, // eslint-disable-line
  key: '$ Requested',
  sortBy: 'RequestedAmount',
}, {
      value: (row) => row.OfferedAmount ? <FormattedNumber value={row.OfferedAmount} style="currency" currency="USD" /> : '-',  // eslint-disable-line
  key: '$ Offered',
  sortBy: 'OfferedAmount',
}, {
  value: 'StatusName',
  key: 'Status',
  sortBy: 'StatusName',
  searchBy: 'StatusName',
}, {
  value: (row) => row.LastProgressDate ? <FormattedDate
    value={new Date(row.LastProgressDate.Time.DateTime)}
    year="numeric"
    month="long"
    day="2-digit"
  /> : '-',
  key: 'Updated',
  sortBy: 'LastProgressDate.Time.Ticks',
  default: 'true',
}];

export class Dashboard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        page: 1,
      },
      items: [],
      partnerId: this.props.auth.user.partnerId,
      filter: this.props.location.pathname.split('/')[2],
    };
  }

  componentDidMount() {
    execute(undefined, undefined, ...'on-boarding-broker/dashboard/get-dashboard-count'.split('/'), {
      partnerId: this.props.auth.user.partnerId,
    })
    .then(({ body }) => {
      this.setState({ dashboardMatrix: body });
      return body;
    }).catch((e) => {
      authCheck(e);
    });
    this.applyTag(this.state.filter);
  }
  handleEnterKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.state.grid.search(this.state.searchField);
    }
  }
  applyTag(tag) {
    this.setState({ tag });
    if (tag === 'active') {
      this.state.filters.raw = {
        StatusName: {
          $nin: ['Declined', 'Funded', 'Not Interested', 'Discarded'],
        },
      };
    } else if (tag === 'closed') {
      this.state.filters.raw = {
        StatusName: {
          $in: ['Declined', 'Funded', 'Not Interested'],
        },
      };
    } else {
      this.state.filters.raw = {};
    }
    this.state.filters.raw.PartnerId = this.state.partnerId;
    this.state.grid.loadGridData(this.state.filters.raw);
  }
  render() {
    return (<Layout>
      <div className="containt_wraper">
        <h2><FormattedMessage {...messages.header} /></h2>
        <ApplicationMatrix dashboardMatrix={this.state.dashboardMatrix} />
        <div className="dashboard_data" > <div className="row">
          <div className=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="status_wraper">
              <div className="row">
                <div className=" col-lg-3 col-md-3 col-sm-6 col-xs-12 no-pad ">
                  <div className="table_searchdata">
                    <div className="input-group search-d">
                      <input onKeyPress={(e) => this.handleEnterKeyPress(e)} onChange={(e) => this.setState({ searchField: e.target.value })} type="text" className="form-control" aria-describedby="basic-addon2" />
                      <span className="input-group-addon"> <button type="button" onClick={() => this.state.grid.search(this.state.searchField)}>Search</button> </span>
                    </div>
                  </div>
                </div>
                <div className=" col-lg-3 col-md-3  hidden-sm col-xs-12"></div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag()} className={!this.state.tag ? 'active active-btn' : 'deactive-btn'}>All</Link>
                </div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag('active')} className={this.state.tag === 'active' ? 'active active-btn' : 'deactive-btn'}>Active</Link>
                </div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag('closed')} className={this.state.tag === 'closed' ? 'active active-btn' : 'deactive-btn'}>Closed</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div className="row">
            <div className=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <DataGrid getRef={(grid) => { this.state.grid = grid; }} onRowClicked={(row) => browserHistory.replace(prepareRedirectionUrl(row.ApplicationNumber, row.StatusName))} columns={columnConfiguration} items="on-boarding-broker/dashboard/get-dashboard-grid" noRecordsMessage="No Applications Found" paginationLabel="Showing applications" />
            </div>
          </div>
        </div>
      </div>
    </Layout>);
  }
}

Dashboard.propTypes = {
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Dashboard: makeSelectDashboard(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dashboard));
