/*
 * Application Messages
 *
 * This contains all the text for the Application component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Application.header',
    defaultMessage: 'This is Application container !',
  },
  requestedAmountPlaceholder: {
    id: 'app.containers.Application.requestedAmount',
    defaultMessage: 'Requested Amount',
  },
  requestedAmountLabel: {
    id: 'app.containers.Application.requestedAmount',
    defaultMessage: 'Requested Amount',
  },
});
