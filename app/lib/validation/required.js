import isEmpty from './isEmpty';

export const required = (name) => (value) => !isEmpty(value) ? undefined : `${name} is required`;
