/**
*
* BusinessDetails
*
*/

import React from 'react';
// import styled from 'styled-components';
import TextField from '@ui/TextField';
import StateField from '@ui/StateField';
import PhoneField from '@ui/PhoneField';
import ZipField from '@ui/ZipField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import messages from './messages';


class BusinessDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const parcedMessages = localityNormalizer(messages);
    return (
      <div className="comman_application_wraper">
        <div className="col-sm-12">
          <h4>Business Details</h4>
        </div>
        <div className="comman_applicationinner business-details">
          <div className="col-md-4 col-sm-4">
            <TextField name="businessLegalName" {...parcedMessages.businessLegalName} />
          </div>
          <div className="col-md-4 col-sm-4">
            <TextField name="dba" {...parcedMessages.dba} />
          </div>
          <div className="col-md-4 col-sm-4">
            <TextField name="ein" {...parcedMessages.ein} />
          </div>
        </div>
        <div className="comman_applicationinner business-details">
          <div className="col-md-4 col-sm-4">
            <TextField name="businessAddress" {...parcedMessages.businessAddress} />
          </div>
          <div className="col-md-4 col-sm-4">
            <TextField name="city" {...parcedMessages.city} />
          </div>
          <div className="col-md-4 col-sm-4">
            <div className="col-xs-6 no-pad-left">
              <StateField name="state" {...parcedMessages.state} />
            </div>
            <div className="col-xs-6 no-pad-left">
              <ZipField name="zipCode" {...parcedMessages.zipCode} />
            </div>
          </div>
        </div>
        <div className="comman_applicationinner business-details">
          <div className="col-md-4 col-sm-4">
            <PhoneField name="officePhone" {...parcedMessages.officePhone} />
          </div>
          <div className="col-md-4 col-sm-4">
          </div>
          <div className="col-md-4 col-sm-4">
          </div>
        </div>
      </div>
    );
  }
}

BusinessDetails.propTypes = {

};

export default BusinessDetails;
